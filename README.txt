/**
 * uc_pagamento_digital: Adds integration with the Pagamento Digital, a brazilian payment gateway.
 * Compatible with Drupal 7.x and Ubercart 3.x
 *
 * By Wanderson S. Reis aka wasare [http://www.ospath.com]
 * Version 2012/07/11
 * Licensed under the GPL version 2
 */

-- SUMMARY --

Pagamento Digital é um módulo que integra o sistema de pagamentos de mesmo nome
ao sistema de loja Ubercart (http://www.ubercart.org/what_is_ubercart).

Novas versões em http://drupal.org/project/uc_pagamento_digital.


-- REQUIREMENTS --

Ubercart 3.x.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

Criar uma conta no Pagamento Digital (https://www.pagamentodigital.com.br/),
com os dados da nova conta preencher as informações necessárias em Administrar ›
Store administration › Configuration › Payment settings.

Não esqueça de habilitar o pagamento digital como método de pagamento.


-- CUSTOMIZATION AND TROUBLESHOOTING --

Para verificar quais e como os dados estão sendo enviados para o pagamento digital
habilite a exibição dos valores enviados pelo formulário (opção de debug).

O retorno dos dados pode ser testado em https://www.pagamentodigital.com.br/checkout/pay/teste.php


-- CONTACT --

Current maintainer:
* Wanderson S. Reis aka wasare - http://drupal.org/user/18262

This project has been sponsored by ospath.com


-- NOTES --

None.
